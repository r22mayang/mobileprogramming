//No 1
goldenFunction = () => {
    console.log("this is golden!!")
}
goldenFunction();

//No 2
const fullname = 'William Imoh'
console.log(fullname)

//No 3
let newObject = {
    firstName: 'Harry',
    lastName: 'Potter Holt',
    destination: 'Hogwarts React Conf',
    occupation: 'Deve-Wizard Avocado'
};
const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

//No 4
let combined = ['Will', 'Chris', 'Sam', 'Holly', 'Gill', 'Brian', 'Noel', 'Maggie']
console.log(combined)

//No 5
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)
