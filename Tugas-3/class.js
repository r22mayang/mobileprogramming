//No 1 
//Release 0
class Animal {
    constructor(name){
       this.name = 'shaun';
       this.legs = 4;
       this.cold_blooded = 'false';
    }
    getaname(){
        return this.name;
    }
}
var sheep = new Animal("shaun");
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

//Release 1
//Class Ape
class Ape extends Animal{
    constructor(name, yell){
        this.name = 'kera sakti';
        this.yell = 'Auooo';
    }
    getnew(){
        return this.yell  ;
    }
}
var sungokong = new Ape("kera sakti");
console.log(sungokong.yell)

//Class Frog
class Frog extends Animal{
    constructor(name, jump){
        this.name = 'buduk';
        this.jump = 'hop hop';
    }
    getnew(){
        return this.jump  ;
    }
}
var kodok = new Frog("buduk");
console.log(kodok.jump)

//No 2
