//if else
var nama = 'John';
var peran = '';
if (nama = ''){
    console.log ("Halo John!");
} else if (peran = ''){
    console.log ("Peranmu werewolf");
} else {
    console.log ("Nama harus diisi!");
}

var nama = 'John';
var peran = '';
if (nama = 'John'){
    console.log ("Halo John, Pilih peranmu untuk memulai game!");
} else if (peran = ''){
    console.log ("Peranmu guard");
} else {
    console.log ("Pilih peranmu");
}


var nama = 'Jane';
var peran = 'Penyihir';
if (nama == 'Jane' || peran == 'Penyihir'){
    console.log ("Selamat datang di Dunia Werewolf, Jane. Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!");
} else {
    console.log ("Peranmu sebagai werewolf");
}


var nama = 'Jenita';
var peran = 'Guard';
if (nama == 'Jenita' || peran == 'Guard'){
    console.log ("Selamat datang di Dunia Werewolf, Jenita. Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf");
} else {
    console.log ("Peranmu sebagai werewolf");
}


var nama = 'Junaedi';
var peran = 'Werewolf';
if (nama == 'Junaedi' || peran == 'Werewolf'){
    console.log ("Selamat datang di Dunia Werewolf, Junaedi. Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
} else {
    console.log ("Peranmu sebagai guard");
}