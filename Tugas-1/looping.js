//Looping While
console.log ('LOOPING PERTAMA');
var i = 1;
while (i <= 20){
    if (i % 2 == 0){
         console.log(i + ' - ' + 'I Love Coding');
    } 
    i++;
}

console.log ('\nLOOPING KEDUA');
var i = 20;
while (i >= 1){
    if (i % 2 == 0){
         console.log(i + ' - ' + 'I Will Become a Mobile Developer');
    } 
    i--;
}

//Looping For
var i = 1;
for (i=1; i<=20; i++){
    if (i % 3 == 0 && i % 2 == 1){
        console.log(i + ' - ' + 'I Love Coding');
    } else if (i % 2 == 0){
        console.log(i + ' - ' + 'Informatika');
    } else if (i % 2 == 1){
        console.log(i + ' - ' + 'Teknik');
    } else {
        console.log('Bilangan Salah');
    }
}

//Membuat Persegi Panjang # 
var s = '';
    for(var i=1; i<=4; i++){
       for(j=1; j<=8; j++){           
       s += '# ';
    }
    s += '\n';
}
console.log(s);
    
//Membuat Tangga
var s = '';
    for(var i=0; i<7; i++){
       for(j=0; j<=i; j++){           
       s += '# ';
    }
    s += '\n';
}
console.log(s);

//Membuat Papan Catur
var s = '';
var p = 8;
var l = 8;
for (var i = 1; i <= l; i++){
    if (i % 2 == 0){
        for (var j = 1; j <= p; j++){
            if (j % 2 == 0){
                s += '#';
            } else {
                s += ' ';
        } 
    }
} else {
         for (var j = 1; j <= p; j++){
             if (j % 2 == 0){
                 s += ' ';
             } else {
                s += '#';
            }
        }
    }
    s += '\n';
}
console.log(s);