//No 1 (Range)
function range(startNum, finishNum){
    var arr = [ ];
    if (startNum == null || finishNum == null){
      return "[-1]";
    } else {
        if (startNum < finishNum) {
          for (var i = startNum; i <= finishNum; i++){
            arr.push(i);
          }
        } else {
          for (var i = startNum; i >= finishNum; i--){
            arr.push(i);
          }
        }
        return arr;
      }
}
console.log (range(1, 10))
console.log (range(1))
console.log (range(11, 18))
console.log (range(54, 50))
console.log (range())

//No 2
function rangewithStep(startNum, finishNum, step=1){
  var arr = [];
  if (startNum == null || finishNum == null){
  } else {
      if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step){
          arr.push(i);
        }
      } else {
        for (var i = startNum; i >= finishNum; i -= step){
          arr.push(i);
        }
      }
      return arr;
    }
}
console.log(rangewithStep(1, 10, 2))
console.log(rangewithStep(11, 23, 3))
console.log(rangewithStep(5, 2, 1))
console.log(rangewithStep(29, 2, 4))

//No 3
function sum(startNum, finishNum, step=1){
  var arr = [];
  if (startNum == null || finishNum == null){
    return "1";
  } else {
      if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step){
          arr.push(i);
        }
      } else {
        for (var i = startNum; i >= finishNum; i -= step){
          arr.push(i);
        }
      }
      return arr.reduce((a,b)=>a+b);
    }
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

//No 4
function dataHandling(data){
  for(var i=0; i<data.length; i++){
    console.log("Nomor ID     : " + data[i][0])
    console.log("Nama Lengkap : " + data[i][1])
    console.log("TTL          : " + data[i][2] + " " + data[i][3])
    console.log("Hobi         : " + data[i][4] + "\n")
    }
    return " ";
  }   
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ]
  console.log(dataHandling(input)) 

  //No 5
  function balikKata(str){
    var string="  ";
   
    for( var i = str.length-1; i>=0; i--){
      string += str[i];
    }
    return string;
  }
  console.log(balikKata("Kasur Rusak"))
  console.log(balikKata("Informatika"))
  console.log(balikKata("Haji Ijah"))
  console.log(balikKata("racecar"))
  console.log(balikKata("I am Humanikers"))
