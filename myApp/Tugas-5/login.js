import React from 'react';
import { View, Image, StyleSheet, Text, TextInput, FlatList, TouchableOpacity, Button, ImageBackground } from 'react-native';

export default class Login extends React.Component{

  render(){
    const [value, onChangeText] = ('');
    const [value1, onChangeText1] = ('');

    return(
    <ImageBackground source={require('/assets/bgg.jfif')} style={styles.bg}>

      <Image style={styles.logo} source={require('/assets/icon.png')}/>
      <Text style = {styles.text}> MY PORTOFOLIO </Text>
      <Text style = {styles.text1}> Log into {"\n"} your account</Text>

    <View style = {styles.input}>
      <TextInput placeholder = 'Email' 
        style={{ height: 30, marginVertical: 10, placeholderTextColor : 'gray' }}
          onChangeText={text => onChangeText(text)} value={value}/>

      <View style={{borderBottomColor: 'darkgray', borderBottomWidth: 1,}}/>

      <TextInput placeholder = 'Password' secureTextEntryplaceholder='Password'
        style={{ height: 30, marginVertical : 10, placeholderTextColor : 'gray' }}
         onChangeText={text => onChangeText(text)} value={value}/>
    </View> 
     
    <View style = {styles.loginbtn} >
      <TouchableOpacity>
        <Text style={{color: 'white',fontSize:15}}> Login </Text>
      </TouchableOpacity>
    </View>
     
    <View style = {styles.reg} >
      <Text> Don't have an account ? 
      <TouchableOpacity>
      <Text> Register </Text>
      </TouchableOpacity></Text>
    </View>

    <Text style = {styles.or}> Or login with your social media </Text>

  <View style = {styles.con1}>
    <TouchableOpacity>
      <Image style = {styles.insta} source = {require('./assets/instagram.png')}/>    
    </TouchableOpacity>

    <TouchableOpacity>
      <Image style={styles.google} source = {require('./assets/google.png')}/>   
    </TouchableOpacity>

    <TouchableOpacity>
      <Image style = {styles.fb} source = {require('./assets/fb.png')}/>    
    </TouchableOpacity>
    
    <TouchableOpacity>
      <Image style = {styles.linkedin} source = {require('./assets/linkedin.png')}/>    
    </TouchableOpacity>
  </View>

   </ImageBackground>
  );
  }
}
    
const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
  },
  bg: {
   
  },
  logo: {
    marginTop:15,
    width: 70,
    height: 70,
    alignSelf : "center"
  },
  text : {
    fontSize : 25,
    fontFamily: 'Lucida Console',
    alignSelf : "center",
    fontWeight : "bold"
  },
  text1 : {
    fontSize : 19,
    paddingTop : 25,
    paddingLeft : 20,
    fontWeight:'bold'
  },
  input :{
    backgroundColor : '#D3D3D3',
    margin:15,
    paddingLeft:20,
    paddingRight:20,
    borderRadius : 25
  },
  loginbtn:{
    alignSelf:'center',
    backgroundColor : 'black',
    borderRadius: 15,
    paddingHorizontal: 30,
    paddingVertical:4,
    marginHorizontal:100,
  },
  reg:{
    paddingTop:10,
    alignSelf:'center',
    marginBottom: 20
  },
  or:{
    alignSelf:'center',
    paddingTop:5,
  },
  insta:{
    width: 20,
    height: 20,
    borderRadius:3,
    marginEnd:9
  },
  google:{
    width: 20,
    height: 20,
    borderRadius:3,
    marginEnd:9
  },
  fb:{
    width: 18,
    height: 18,
    borderRadius:3,
    marginEnd:9
  },
  linkedin:{
    width: 19,
    height: 19,
    borderRadius:3
  },
  con1:{
    paddingTop:5,
    alignSelf:'center',
    flexDirection:'row',
    marginBottom:55
  }
});