import React from 'react';
import { View, Image, StyleSheet, Text, TextInput, FlatList, TouchableOpacity, Button, ImageBackground } from 'react-native';

export default class Biodata extends React.Component{
  render(){

    return (
      <ImageBackground source={require('/assets/bgg.jfif')} style={styles.bg}>

      <View>
      <View style={styles.container}>
        <View style={{ height: 100, width:300, paddingLeft: 80}}>
          <View style={{ flexDirection: "row",backgroundColor:'#FFE4E1', flex:1,                        borderRadius:10 }} />
        </View>
      <Text style = {styles.text}> RISKI{'\n'} MAYANG SARI </Text>
      </View>

      <View style={styles.container1}>
        <View style={{ height: 100, width:300, paddingRight: 80}}>
          <View style={{ flexDirection: "row",backgroundColor: '#D3D3D3',flex:1,                        borderRadius:10 }} />
        </View>
      <Text style = {styles.text1}> TEKNIK INFORMATIKA </Text>
      </View>

      <View style={styles.container2}>
        <View style={{ height: 100, width:300, paddingLeft: 80}}>
          <View style={{ flexDirection: "row",backgroundColor: '#000000',flex:1,                        borderRadius:10 }} />
        </View>
      <Text style = {styles.text2}> PAGI C </Text>
      </View>

      </View>

    </ImageBackground>
    );
  }
}

 const styles = StyleSheet.create({
   container:{
     paddingTop:10,
     paddingRight:2,
     alignSelf:"flex-end",
   },
   container1:{
     alignSelf:"flex-start",
     paddingLeft:2,
   },
   container2:{
     alignSelf:"flex-end",
     paddingRight:2,
   },
  text: {
    alignSelf:"flex-start",
    paddingLeft:10,
    paddingBottom:10,
    paddingTop:10,
    fontFamily:"Gramond"
  },
  text1:{
    alignSelf:'flex-end',
    paddingTop:10,
    paddingRight:10,
    paddingBottom:10,
    fontFamily:"Gramond"
  },
  text2: {
    alignSelf:"flex-start",
    paddingLeft:10,
    paddingBottom:10,
    paddingTop:10,
    paddingBottom:70,
    fontFamily:"Gramond"
  },
 });